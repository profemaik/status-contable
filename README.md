# Estatus Contable

[![Captura de Pantalla Estatus Contable][product-screenshot]](<https://gitlab.com/profemaik/status-contable>)

Aplicación Web para el seguimiento de gastos e ingresos financieros personales.

## Tabla de Contenidos

1. [Construido con](#construido-con)
2. [Primeros Pasos](#primeros-pasos)
3. [Uso](#uso)
4. [Hoja de Ruta](#hoja-de-ruta)
5. [Contribuir](#contribuir)
6. [Licencia](#licencia)
7. [Contacto](#contacto)
8. [Agradecimientos](#agradecimientos)

## Construido con

* Back-end
  * [Laravel 9](https://laravel.com/)
  * [Laravel Jetstream](https://jetstream.laravel.com/)
  * [PostgreSQL](https://www.postgresql.org/)
* Front-end
  * [Inertia.js](https://inertiajs.com/)
  * [Vue.js](https://vuejs.org/)
  * [Bulma](https://bulma.io/)
  * [bulma-dracula](https://mazipan.github.io/bulma-dracula/)

## Primeros Pasos

### Con Docker

1. ```sh
   git clone https://gitlab.com/profemaik/status-contable.git
   ```

2. ```sh
   docker run --rm \
        -u "$(id -u):$(id -g)" \
        -v $(pwd):/var/www/html \
        -w /var/www/html \
        laravelsail/php81-composer:latest \
        composer install --ignore-platform-reqs
   ```

3. ```sh
   # Si tiene configurado el alias para Sail de Laravel
   sail up -d

   # Si no tiene configurado el alias para Sail de Laravel
   ./vendor/bin/sail up -d
   ```

4. Abra la URL `http://localhost` en el navegador web.

### Con PHP y Composer

_Prerequisitos_

* PHP 8.0 o superior.
* Servidor de Base de Datos PostgreSQL 14 o superior.

1. ```sh
   git clone https://gitlab.com/profemaik/status-contable.git
   ```

2. ```sh
   composer install
   ```

3. ```sh
   php artisan serve
   ```

4. Abra la URL `http://localhost` en el navegador web.

## Uso

1. Regístrese como usuario en la aplicación.
2. Cree, al menos, una cuenta.
3. Registre movimientos de créditos o débitos (ingresos y gastos respectivamente).
4. El tablero generará las gráficas para visualizar los créditos (ingresos) y los débitos (gastos) del mes actual.

## Hoja de Ruta

Eche un vistazo a las [incidencias](https://gitlab.com/profemaik/status-contable/-/issues) para que conozca nuevas funcionalidades propuestas y/o problemas conocidos. Siéntase libre de
solicitar nuevas funcionalidades o aportar nuevas ideas.

## Contribuir

Las contribuciones son las que hacen que la comunidad de código abierto sea un lugar tan increíble para aprender, inspirar y crear. Cualquier contribución que haga es **muy apreciada**.

1. Realice un fork del proyecto
2. Cree su rama feature (`git checkout -b feature/NuevaFuncionalidad`)
3. Confirme sus cambios (`git commit -m 'Agrega nueva funcionalidad'`)
4. Ejecute un Push a la rama (`git push origin feature/NuevaFuncionalidad`)
5. Abra una petición de fusión (Merge)

## Licencia

Se distribuye bajo la Licencia MIT. Lea `LICENSE` para más información.

## Contacto

[Maikel Carballo][portfolio] - [@\_kimael_][twitter]

Enlace del Proyecto: [SCBmk](https://gitlab.com/profemaik/status-contable)

## Agradecimientos

* [mazipan](https://mazipan.github.io/bulma-dracula/)

[product-screenshot]: public/images/Screenshot_20220406_213341.webp
[portfolio]: https://kimael-code.github.io/mi-portafolio/
[twitter]: https://twitter.com/_kimael_
