<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Inertia\Inertia;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filters = FacadesRequest::only('search');
        $accounts = $this->datesForHumans(
            Account::where('user_id', Auth::id())
                   ->filter($filters)
                   ->latest()
                   ->paginate()
        );

        return Inertia::render('Accounts/Index', [
            'filters'  => FacadesRequest::all('search'),
            'accounts' => fn () => $accounts,
        ]);
    }

    private function datesForHumans($accounts)
    {
        $accounts->map(function ($account)
        {
            $account['diff_created_at'] = $account->created_at->diffForHumans();
            $account['diff_updated_at'] = $account->updated_at->diffForHumans();
            $account['diff_deleted_at'] = $account->deleted_at ?
                $account->deleted_at->diffForHumans() :
                null;
            return $account;
        });

        return $accounts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Accounts/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'     => ['required', 'string', 'max:255', 'unique:accounts',],
            'currency' => ['required', 'string', 'max:8',],
            'balance'  => ['required', 'numeric', 'min:0.01', 'max:99999999.99'],
        ]);

        if ($request->balance < 0) {
            $msg = 'Imposible crear una nueva cuenta con saldo negativo';

            return Redirect::route('accounts.index')->with('error', $msg);
        }

        $userAuthenticated = User::find(Auth::id());

        DB::transaction(function () use ($request, $userAuthenticated)
        {

            $account = new Account($request->only('name', 'currency', 'balance'));

            $userAuthenticated->accounts()->save($account);
        });

        $msg = "Cuenta <strong>"
             . $request->name
             . "</strong> creada con éxito. ";

        return Redirect::route('accounts.index')->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        $account = $this->datesForHumans(
            Account::where('id', $account->id)->get()
        );

        foreach ($account as $row)
        {
            $account = $row;
        }

        return Redirect::back()->with('modal', $account);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        //
    }
}
