<?php

namespace App\Http\Controllers;

use App\Models\AccountMovement;
use Illuminate\Http\Request;

class AccountMovementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AccountMovement  $accountMovement
     * @return \Illuminate\Http\Response
     */
    public function show(AccountMovement $accountMovement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AccountMovement  $accountMovement
     * @return \Illuminate\Http\Response
     */
    public function edit(AccountMovement $accountMovement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AccountMovement  $accountMovement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccountMovement $accountMovement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AccountMovement  $accountMovement
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccountMovement $accountMovement)
    {
        //
    }
}
