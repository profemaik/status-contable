<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountMovement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Inertia\Inertia;

class CreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filters = FacadesRequest::only('search', 'account_id');
        $credits = $this->datesForHumans(
            AccountMovement::join('accounts', 'account_movements.account_id', '=', 'accounts.id')
                            ->select('account_movements.*', 'accounts.name', 'accounts.currency', 'accounts.balance')
                            ->where('accounts.user_id', Auth::id())
                            ->where('mov_type', AccountMovement::MOV_TYPE_CREDIT)
                            ->filter($filters)
                            ->latest('account_movements.created_at')
                            ->paginate()
        );

        return Inertia::render('Credits/Index', [
            'filters'  => FacadesRequest::all('search', 'account_id'),
            'accounts' => Account::where('user_id', Auth::id())
                                    ->orderBy('name')
                                    ->get(),
            'credits'  => fn () => $credits,
        ]);
    }

    private function datesForHumans($credits)
    {
        $credits->map(function ($credit)
        {
            $credit['diff_made_at'] = $credit->made_at->diffForHumans();
            $credit['diff_created_at'] = $credit->created_at->diffForHumans();
            $credit['diff_updated_at'] = $credit->updated_at->diffForHumans();

            if ($credit->deleted_at) {
                $credit['diff_deleted_at'] = $credit->deleted_at->diffForHumans();
            }

            return $credit;
        });

        return $credits;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Credits/Create', [
            'accounts' => Account::where('user_id', Auth::id())
                                    ->orderBy('name')
                                    ->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'account_id' => ['required', 'exists:accounts,id',],
            'mov_amount' => ['required', 'numeric', 'between:0.01,99999999.99'],
            'about'      => ['required', 'min:3', 'max:2048',],
            'made_at'    => ['required', 'date',],
        ]);

        $accountSelected = Account::find($request->account_id);

        DB::transaction(function () use ($request, $accountSelected)
        {

            $accountMovement = new AccountMovement([
                'mov_type'    => AccountMovement::MOV_TYPE_CREDIT,
                'mov_amount'  => $request->mov_amount,
                'mov_balance' => $accountSelected->balance + $request->mov_amount,
                'about'       => $request->about,
                'made_at'     => $request->made_at,
            ]);

            $accountSelected->accountMovements()->save($accountMovement);
            $accountSelected->balance += $request->mov_amount;
            $accountSelected->save();
        });

        $msg = "Crédito por monto de <strong>"
             . number_format($request->mov_amount, 2, ',', '.')
             . "</strong> registrado a la cuenta "
             . "<em>" . $accountSelected->name . "</em>.";

        return Redirect::route('credits.index')->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $credit = $this->datesForHumans(
            AccountMovement::with('account')->where('id', $id)->get()
        );

        foreach ($credit as $row)
        {
            $credit = $row;
        }

        return Redirect::back()->with('modal', $credit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
