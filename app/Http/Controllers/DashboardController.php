<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountMovement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index()
    {
        $currentMonth = Carbon::now()->month;
        $movTypeDebit = AccountMovement::MOV_TYPE_DEBIT;
        $movTypeCredit = AccountMovement::MOV_TYPE_CREDIT;

        return Inertia::render('Dashboard', [
            'accounts' => Account::where('user_id', Auth::id())
                                 ->orderBy('name')
                                 ->get(),
            'debits'   => AccountMovement::join('accounts', 'account_movements.account_id', '=', 'accounts.id')
                                         ->whereMonth('account_movements.made_at', $currentMonth)
                                         ->where('accounts.user_id', Auth::id())
                                         ->where('mov_type', $movTypeDebit)
                                         ->get(),
            'credits'  => AccountMovement::join('accounts', 'account_movements.account_id', '=', 'accounts.id')
                                         ->whereMonth('account_movements.made_at', $currentMonth)
                                         ->where('accounts.user_id', Auth::id())
                                         ->where('mov_type', $movTypeCredit)
                                         ->get(),
        ]);
    }
}
