<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountMovement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Inertia\Inertia;

class DebitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filters = FacadesRequest::only('search', 'account_id');
        $debits = $this->datesForHumans(
            AccountMovement::join('accounts', 'account_movements.account_id', '=', 'accounts.id')
                            ->select('account_movements.*', 'accounts.name', 'accounts.currency', 'accounts.balance')
                            ->where('accounts.user_id', Auth::id())
                            ->where('mov_type', AccountMovement::MOV_TYPE_DEBIT)
                            ->filter($filters)
                            ->latest('account_movements.created_at')
                            ->paginate()
        );

        return Inertia::render('Debits/Index', [
            'filters'  => FacadesRequest::all('search', 'account_id'),
            'accounts' => Account::where('user_id', Auth::id())
                                    ->orderBy('name')
                                    ->get(),
            'debits'  => fn () => $debits,
        ]);
    }

    private function datesForHumans($debits)
    {
        $debits->map(function ($debit)
        {
            $debit['diff_made_at'] = $debit->made_at->diffForHumans();
            $debit['diff_created_at'] = $debit->created_at->diffForHumans();
            $debit['diff_updated_at'] = $debit->updated_at->diffForHumans();

            if ($debit->deleted_at) {
                $debit['diff_deleted_at'] = $debit->deleted_at->diffForHumans();
            }

            return $debit;
        });

        return $debits;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Debits/Create', [
            'accounts' => Account::where('user_id', Auth::id())
                                    ->orderBy('name')
                                    ->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'account_id' => ['required', 'exists:account_movements',],
            'mov_amount' => ['required', 'numeric', 'between:0.01,99999999.99'],
            'about'      => ['required', 'min:3', 'max:2048',],
            'made_at'    => ['required', 'date',],
        ]);

        $accountSelected = Account::find($request->account_id);

        if (($accountSelected->balance - $request->mov_amount) < 0) {
            $msg = 'Fondos insuficientes';

            return Redirect::route('debits.index')->with('error', $msg);
        }

        DB::transaction(function () use ($request, $accountSelected)
        {

            $accountMovement = new AccountMovement([
                'mov_type'    => AccountMovement::MOV_TYPE_DEBIT,
                'mov_amount'  => $request->mov_amount,
                'mov_balance' => $accountSelected->balance - $request->mov_amount,
                'about'       => $request->about,
                'made_at'     => $request->made_at,
            ]);

            $accountSelected->accountMovements()->save($accountMovement);
            $accountSelected->balance -= $request->mov_amount;
            $accountSelected->save();
        });

        $msg = "Débito por monto de <strong>"
             . number_format($request->mov_amount, 2, ',', '.')
             . "</strong> registrado a la cuenta "
             . "<em>" . $accountSelected->name . "</em>.";

        return Redirect::route('debits.index')->with('success', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $debit = $this->datesForHumans(
            AccountMovement::with('account')->where('id', $id)->get()
        );

        foreach ($debit as $row)
        {
            $debit = $row;
        }

        return Redirect::back()->with('modal', $debit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
