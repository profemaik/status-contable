<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'currency',
        'balance',
    ];

    protected $casts = [
        'balance' => 'decimal:2',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function accountMovements()
    {
        return $this->hasMany(AccountMovement::class);
    }

    /**
     * Filtros para las búsquedas rápidas.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilter($query, array $filters)
    {
        return $query->when($filters['search'] ?? null, function ($query, $search)
        {
            $query->where(function ($query) use ($search)
            {
                $query->where('name', 'ilike', '%' . $search . '%')
                      ->orWhere('currency', 'ilike', '%' . $search . '%');
            });
        })->when($filters['trashed'] ?? null, function ($query, $trashed)
        {
            if ($trashed === 'with')
            {
                $query->withTrashed();
            }
            elseif ($trashed === 'only')
            {
                $query->onlyTrashed();
            }
        });
    }
}
