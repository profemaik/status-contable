<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountMovement extends Model
{
    use HasFactory;

    public const MOV_TYPE_CREDIT = 'C';
    public const MOV_TYPE_DEBIT = 'D';

    protected $fillable = [
        'mov_type',
        'mov_amount',
        'mov_balance',
        'about',
        'made_at',
    ];

    protected $casts = [
        'mov_amount'  => 'decimal:2',
        'mov_balance' => 'decimal:2',
        'made_at'     => 'datetime',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * Filtros para las búsquedas rápidas.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilter($query, array $filters)
    {
        return $query->when($filters['search'] ?? null, function ($query, $search)
        {
            $query->where(function ($query) use ($search)
            {
                $query->where('about', 'ilike', '%' . $search . '%');
            });
        })->when($filters['trashed'] ?? null, function ($query, $trashed)
        {
            if ($trashed === 'with')
            {
                $query->withTrashed();
            }
            elseif ($trashed === 'only')
            {
                $query->onlyTrashed();
            }
        })->when($filters['account_id'] ?? null, function ($query, $accountID)
        {
            $query->where('account_id', $accountID);
        });
    }
}
