<?php

namespace Database\Factories;

use App\Models\Account;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Account>
 */
class AccountFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Account::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'  => function (array $attributes) {
                return User::find($attributes['user_id'])->id;
            },
            // 'user_id'  => User::factory(),
            'name'     => $this->faker->words(3, true),
            'currency' => $this->faker->randomElement(['VES', 'USD', 'BTC']),
            'balance'  => $this->faker->numerify(),
        ];
    }
}
