<?php

namespace Database\Factories;

use App\Models\Account;
use App\Models\AccountMovement;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AccountMovement>
 */
class AccountMovementFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'account_id' => function (array $attributes) {
                return Account::find($attributes['account_id'])->id;
            },
            'mov_type'   => $this->faker->randomElement([
                AccountMovement::MOV_TYPE_CREDIT,
                AccountMovement::MOV_TYPE_DEBIT,
            ]),
            'mov_amount'  => $this->faker->randomFloat(2, 0, 8),
            'mov_balance' => function (array $attributes)
            {
                $movType = $attributes['mov_type'];
                $movAmount = $attributes['mov_amount'];
                $balance = Account::find($attributes['account_id'])->balance;
                return $movType === 'C' ? $balance + $movAmount : $balance - $movAmount;
            },
            'about'       => $this->faker->realText(),
            'made_at'     => $this->faker->dateTime(),
        ];
    }
}
