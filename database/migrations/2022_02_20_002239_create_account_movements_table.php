<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_movements', function (Blueprint $table) {
            $table->id()->generatedAs()->always();
            $table->foreignId('account_id')->constrained();
            $table->string('mov_type', 1);
            $table->decimal('mov_amount');
            $table->decimal('mov_balance');
            $table->string('about', 255);
            $table->timestamp('made_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_movements');
    }
};
