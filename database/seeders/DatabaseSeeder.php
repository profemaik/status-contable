<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\AccountMovement;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(2)
            ->create()
            ->each(function ($user)
            {
                Account::factory(4)
                       ->create(['user_id' => $user->id])
                       ->each(function ($account)
                       {
                            AccountMovement::factory(5)
                                           ->create(['account_id' => $account->id]);
                        });
            });
    }
}
