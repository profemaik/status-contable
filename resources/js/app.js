require('./bootstrap');

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import VueChartkick from 'vue-chartkick'
import 'chartkick/chart.js'
import 'chartjs-adapter-luxon';

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

Chartkick.options = {
    library: {
        scales: {
            x: {
                time: { unit: 'day', tooltipFormat: "f" },
                adapters: { date: { locale: "es-VE" } },
            },
        },
    },
};

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => require(`./Pages/${name}.vue`),
    setup({ el, app, props, plugin }) {
        return createApp({ render: () => h(app, props) })
            .use(plugin)
            .use(VueChartkick)
            .mixin({ methods: { route } })
            .mount(el);
    },
});

InertiaProgress.init({ color: '#4B5563' });
